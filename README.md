# Aide et ressources de Matlab pour Synchrotron SOLEIL

[<img src="https://fr.mathworks.com/etc.clientlibs/mathworks/clientlibs/customer-ui/templates/common/resources/images/pic-header-mathworks-logo.svg" width="500"/>](https://fr.mathworks.com/products/matlab.html)

## Résumé

- Imaging Processing Tb, Statistical, PLS Tb chimiometrie EigenVector (commercial)
- Privé

## Sources

- Code source: gitlab de l'INRA, privé
- Documentation officielle: https://fr.mathworks.com/help/

## Navigation rapide

| Ressources annexes | Tutoriaux |
| ------ | ------ |
| [Ressources officielles](https://fr.mathworks.com/support.html?s_tid=gn_supp) | [Tutoriaux officiels](https://fr.mathworks.com/support/learn-with-matlab-tutorials.html) |
| | [Exemples officiels](https://fr.mathworks.com/help/examples.html) |
| | [Tutoriaux tutorialspoint](https://www.tutorialspoint.com/matlab/index.htm) |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Post-Doc/CDD ne peut pas installer Matlab

## Format de données

- en entrée: NGV, SPC Horiba, GRAMS, TIFF, texte
- en sortie: variable: MAT, TIFF, texte
- sur un disque dur, sur la Ruche, communication trop lente, pb d'identification/acces aux fichiers
